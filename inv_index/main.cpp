#include "inv_index.h"
#include <chrono>
#include <iostream>

using namespace std;

int main() {

  std::string dir;
  cout << "Directory path: ";
  cin >> dir;

  inv_index index(dir);

  unsigned int thnum;
  cout << "Threads num: ";
  cin >> thnum;
  index.set_threads_count(thnum);
  auto start = std::chrono::high_resolution_clock::now();
  index.create_inverted_index();
  auto end = std::chrono::high_resolution_clock::now();
  auto time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
                  .count();
  cout << "Build in " << time << " ms";

  char f = 'n';
  cout << "\nWrite index to file [y\\n]: ";
  cin >> f;
  if (f == 'y') {
    string fname;
    cout << "File name: ";
    cin >> fname;
    index.write_to_file(fname);
  }
  string words;
  cout << "Enter words to find: ";
  cin >> words;

  auto res = index.get_index();
  auto dirs = index.find(words);
  for (auto c : dirs) {
    cout << c << endl;
  }

  return 0;
}
