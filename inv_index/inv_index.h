
#ifndef INVERTEDINDEX_H
#define INVERTEDINDEX_H
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <map>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <utility>
#include <vector>

using namespace std;

class inv_index {
  string filepath;   
  unsigned int tnum; 
  mutex m;

  void get_file_list(); 
  map<const string, set<unsigned int>>index; 

  set<string> parser(const string &text); 
  string read_file(const string &file_path);

  void process(const vector<pair<string, unsigned int>>
                   &); 
  set<unsigned int> intersection(
      const set<unsigned int> &s1, 
                                   
      const set<unsigned int> &s2);

public:
  vector<pair<string, unsigned int>>dirs; 
  inv_index(){};
  inv_index(string path) : filepath(path) {}

  auto get_index() const { return index; }
  string get_filepath() const { return filepath; }
  void set_filepath(const string &value) { filepath = value; }
  void set_threads_count(unsigned int num) { tnum = num; }

  void write_to_file(const string &fname);

  void create_inverted_index();
  vector<string> find(const string &text); 
    
};

#endif // INVERTEDINDEX_H
