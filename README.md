**course_work_parallel_computing**
Programm builds simple inverted index of .txt files in single-thread mode or in multi-thread mode (from 1 to N threads)

**Prerequisites**
Instaled  Code::Blocks 20.03

**Instalation and running**

1. Create new C++ project
2. Add three new files: inv_index.cpp, inv_index.h and main.cpp . Copy the corresponding code in them from this repository inv_index
3. Create new folder in the root of the project with name test_dataset
4. Upload folders with your .txt files into folders test_dataset
5. Build the project 
6. Run the project
7. Follow the instructions


